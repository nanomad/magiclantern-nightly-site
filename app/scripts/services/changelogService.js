'use strict';

function ChangelogService($dialog) {

	var dialogDefaults = {
		backdrop: true,
		keyboard: true,
		backdropClick: true,
		dialogFade: true,
		templateUrl: 'views/changelog.html'
	};

	this.show = function (build) {
		//Create temp objects to work with since we're in a singleton service
		var tempDialogDefaults = {};

		//Map angular-ui dialog custom defaults to dialog defaults defined in this service
		angular.extend(tempDialogDefaults, dialogDefaults, {});

		//Attach a controller to the dialog
		tempDialogDefaults.controller = function ($scope, dialog) {
			$scope.build = build;

			$scope.close = function() {
				dialog.close();
			};
		};

		$dialog.dialog(tempDialogDefaults).open();
	};
}

angular.module('nightlyApp').service('ChangelogService', ChangelogService);
