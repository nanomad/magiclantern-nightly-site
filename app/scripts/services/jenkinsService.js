'use strict';

function JenkinsService(Restangular, $http, $q) {
  var apiEndpoint = 'http://builds.magiclantern.fm/jenkins';
  
  return {
    platforms: function() {
      return Restangular.one('view','ML%20Platforms').get().get('jobs');
    },

    builds: function(_platform) {
      return Restangular.one('job',_platform).get({depth: 1}).get('builds');
    },

    downloadURL: function(_platform, _build) {
      if(_build && !_build.building && _build.result === 'SUCCESS') {
        return [apiEndpoint, 'job', _platform.name, _build.number,'artifact', _build.artifacts[0].relativePath].join('/');
      } else {
        return '';
      }
    },

    consoleText: function(_platform, _build) {
      var promise = $q.defer();
      if(!_build || !_build.number || !_platform) {
        promise.resolve('No data');
      } else if(_build.building) {
        promise.resolve('Building ...');
      } else {
        var url = [apiEndpoint, 'job', _platform.name, _build.number, 'consoleText'].join('/');
        $http.get(url).success(function(data) {
          promise.resolve(data);
        });
      }
      return promise.promise;
    }
  };
}

angular.module('nightlyApp').factory('JenkinsService', JenkinsService);
